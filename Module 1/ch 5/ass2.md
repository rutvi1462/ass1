<span style="color:#337ab7; font-size:30px; text-decoration:none">[Computer Science & Engineering](http://vlab.co.in/page-not-found)</span>

## Introduction

# Computer Graphics

Welcome to the Computer Graphics lab ! Here , we will try some virtual experiments to understand the different concepts of graphics. We will go through understanding the concepts of 3D Coordinate systems, Transformations, Hierarchical modelling and Cameras. Later we will be visualising some basic algorithms used to render the graphics.

## List of Experiments

# Computer Graphics

- [Points and Co-ordinate Systems](http://cse18-iiith.vlabs.ac.in/exp1/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)
- [Transformations: Translation](http://cse18-iiith.vlabs.ac.in/exp2/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)
- [Transformations: Rotation](http://cse18-iiith.vlabs.ac.in/exp3/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)
- [Transformations: Scaling](http://cse18-iiith.vlabs.ac.in/exp4/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)
- [Hierarchical Transformations: 2D Demo](http://cse18-iiith.vlabs.ac.in/exp5a/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)
- [Hierarchical Transformations: 3D Articulated Arm](http://cse18-iiith.vlabs.ac.in/exp5b/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)
- [Projections and Cameras](http://cse18-iiith.vlabs.ac.in/exp6/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)
- [Clipping: Line](http://cse18-iiith.vlabs.ac.in/exp7/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)
- [Clipping: Polygon](http://cse18-iiith.vlabs.ac.in/exp8/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)
- [Rasterization: Line](http://cse18-iiith.vlabs.ac.in/exp9/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)
- [Rasterization: Polygon](http://cse18-iiith.vlabs.ac.in/exp10/Introduction.html?domain=Computer%20Science&lab=Computer%20Graphics)

## Target Audience

# Computer Graphics

The experiments are targeted at students with proficiency in writing programs in any preferred programming language. Knowledge of matrix theory and linear algebra is required.

## Cources Aligned

# Computer Graphics

The basic concepts of CG are alignd with courses such as Linear Algebra, Matrix Theory, Projective Geometry and the like. The experiments touch on most topics covered in such courses in most curricula.

## Prerequisite S/W

Prerequisite S/W

  -This lab requires few Software Dependencies to run the Simulations

1. The downloadable CG toolkit provided for first 6 experiments comes prepacked with the required libraries and requires Java 1.7 and JRE 1.7 to run.
2. Browsers Firefox 50 and it's below versions, internet explorer.
3. IcedTea Plugin for Linux Operating System.

-We recommend the following for the smooth running of these simulations

1. Install VirtualBox - Virtual Labs offers a free download of a customized VirtualBox with the required pre installed all the software dependencies(Java 1.7, IcedTea Plugin and Adobe Flash Plugin). Please follow the link to download, install and use VirtualBox for Virtual Labs.

## Feedback

# Computer Graphics

[Feedback](http://feedback.vlabs.ac.in/)