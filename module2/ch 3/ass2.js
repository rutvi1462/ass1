function calculation() {
    //value of the vertices
    var x1 = document.getElementById("x1");
    var y1 = document.getElementById("y1");
    var x2 = document.getElementById("x2");
    var y2 = document.getElementById("y2");
    var x3 = document.getElementById("x3");
    var y3 = document.getElementById("y3");
    var x = document.getElementById("x");
    var y = document.getElementById("y");
    var c1 = +x1.value;
    var d1 = +y1.value;
    var c2 = +x2.value;
    var d2 = +y2.value;
    var c3 = +x3.value;
    var d3 = +y3.value;
    var c = +x.value;
    var d = +y.value;
    if (isNaN(c1) || isNaN(d1) || isNaN(c2) || isNaN(d2) || isNaN(c3) || isNaN(d3) || isNaN(c) || isNaN(d)) {
        alert("please enter numbers...");
    }
    else if (c1 % 1 != 0 || d1 % 1 != 0 || c2 % 1 != 0 || d2 % 1 != 0 || c3 % 1 != 0 || d3 % 1 != 0 || c % 1 != 0 || d % 1 != 0) {
        alert("please enter integer Number.");
    }
    else {
        var ab = Math.sqrt(Math.pow((c2 - c1), 2) + Math.pow((d2 - d1), 2)); //find a
        var ac = Math.sqrt(Math.pow((c1 - c3), 2) + Math.pow((d1 - d3), 2)); //find b
        var bc = Math.sqrt(Math.pow((c3 - c2), 2) + Math.pow((d3 - d2), 2)); //find c
        if ((bc + ac) > ab && (ab + bc) > ac && (ab + ac) > bc) { // check coordinates make a triangle or not
            var s = (ab + bc + ac) / 2;
            var area = Math.sqrt(s * ((s - ab) * (s - bc) * (s - ac))); //find area of triangle
            document.getElementById("area").innerHTML = "area of triangle is : " + area;
            var a1 = Math.sqrt(Math.pow((c - c1), 2) + Math.pow((d - d1), 2));
            var b1 = Math.sqrt(Math.pow((c - c2), 2) + Math.pow((d - d2), 2));
            var c1 = Math.sqrt(Math.pow((c - c3), 2) + Math.pow((d - d3), 2));
            if ((a1 + b1 + c1) < (ab + bc + ac)) // check point is lies inside or not
             {
                document.getElementById("check1").innerHTML = "Point is ";
                document.getElementById("check").innerHTML = "inside";
            }
            else {
                document.getElementById("check1").innerHTML = "Point is ";
                document.getElementById("check").innerHTML = "outside";
            }
        }
        else {
            document.getElementById("area").innerHTML = "This coordinates are not make a triangle";
        }
    }
}
//# sourceMappingURL=ass2.js.map