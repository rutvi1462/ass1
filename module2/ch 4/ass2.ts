function calculation(){
    //value of the vertices
    var x1:HTMLInputElement = <HTMLInputElement>document.getElementById("x1");
    var y1:HTMLInputElement = <HTMLInputElement>document.getElementById("y1");
    var x2:HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
    var y2:HTMLInputElement = <HTMLInputElement>document.getElementById("y2");
    var x3:HTMLInputElement = <HTMLInputElement>document.getElementById("x3");
    var y3:HTMLInputElement = <HTMLInputElement>document.getElementById("y3");
    var x:HTMLInputElement = <HTMLInputElement>document.getElementById("x");
    var y:HTMLInputElement = <HTMLInputElement>document.getElementById("y");

    var c1:number = +x1.value;
    var d1:number = +y1.value;
    var c2:number = +x2.value;
    var d2:number = +y2.value;
    var c3:number = +x3.value;
    var d3:number = +y3.value;
    var c:number = +x.value;
    var d:number = +y.value;

    
    if(isNaN(c1) || isNaN(d1) || isNaN(c2) || isNaN(d2) || isNaN(c3) || isNaN(d3) || isNaN(c) || isNaN(d)){
        alert("please enter numbers...");
    }
    else if(c1 % 1 != 0 || d1 % 1 != 0 || c2 % 1 != 0 || d2 % 1 != 0 || c3 % 1 != 0 || d3 % 1 != 0 || c % 1 != 0 || d % 1 != 0){
        alert("please enter integer Number.");
    }
    else{
        var a = Math.sqrt(Math.pow((c2-c1),2)+Math.pow((d2-d1),2));
        var b = Math.sqrt(Math.pow((c1-c3),2)+Math.pow((d1-d3),2));
        var c = Math.sqrt(Math.pow((c3-c2),2)+Math.pow((d3-d2),2));

        var s = (a+b+c)/2;

        var area = Math.sqrt(s*((s-a)*(s-b)*(s-c)));
        
        document.getElementById("area").innerHTML = "area of triangle is : "+area;

        var a1 = Math.sqrt(Math.pow((c-c1),2)+Math.pow((d-d1),2));
        var b1 = Math.sqrt(Math.pow((c-c2),2)+Math.pow((d-d2),2));
        var c1 = Math.sqrt(Math.pow((c-c3),2)+Math.pow((d-d3),2));
       
        if((a1+b1+c1) < (a+b+c))
        {
            document.getElementById("check").innerHTML = "point is inside";
        }
        else{
            document.getElementById("check").innerHTML = "point is outside";
        }
    }
}